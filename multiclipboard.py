"""Inspired from Automate the Boring Stuff with Pythond
TODO: add function to use load via clicking on slots"""
import sys
import time
import pyperclip

from PyQt4 import QtCore, QtGui


def figureNumber(number):
    if number > 0 and number < 5:
        j = 0
        i = number - j - 1
    elif number > 4 and number < 9:
        j = 1
        i = number - j - 4
    else:
        return 0
    return (i, j)


class Window(QtGui.QMainWindow):
    def __init__(self):
        super(Window, self).__init__()

        # Declerations
        windowIcon = QtGui.QIcon('logo.png')

        # Window self
        self.resize(1024, 768)
        self.setWindowTitle('RinqtMith\'s MultiClipBoard')
        self.setWindowIcon(windowIcon)
        self.setFixedSize(1024, 768)

        # Layout
        self.handleText = HandleText()

        widget = QtGui.QWidget()
        layout = QtGui.QHBoxLayout(widget)
        layout.addWidget(self.handleText)

        self.setCentralWidget(widget)

        # GUI init
        self.initUI()

    def initUI(self):
        # Declerations
        quitIcon = QtGui.QIcon('Exit-icon.png')
        infoIcon = QtGui.QIcon('Info-icon.png')
        saveIcon = QtGui.QIcon('Save-icon.png')
        helpIcon = QtGui.QIcon('Comment-icon.png')
        loadIcon = QtGui.QIcon('Load-icon.png')

        # Actions
        exitAction = QtGui.QAction(quitIcon, '&Quit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Quit the application')
        exitAction.triggered.connect(self.close)

        aboutAction = QtGui.QAction(infoIcon, '&About', self)
        aboutAction.setStatusTip('About')
        aboutAction.triggered.connect(self.about)

        saveAction = QtGui.QAction(saveIcon, '&Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Save from clipboard')
        saveAction.triggered.connect(self.saveme)

        loadAction = QtGui.QAction(loadIcon, '&Load', self)
        loadAction.setShortcut('Ctrl+O')
        loadAction.setStatusTip('Load from selected slot')
        loadAction.triggered.connect(self.loadme)

        helpAction = QtGui.QAction(helpIcon, '&Help', self)
        helpAction.setStatusTip('Help')
        helpAction.triggered.connect(self.helpme)

        # Statusbar
        statusbar = self.statusBar()
        statusbar.showMessage('Ready!')
        statusbar.setSizeGripEnabled(False)

        # Menubar
        menubar = self.menuBar()

        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(saveAction)
        fileMenu.addAction(loadAction)
        fileMenu.addAction(exitAction)

        aboutMenu = menubar.addMenu('&About')
        aboutMenu.addAction(aboutAction)
        aboutMenu.addAction(helpAction)

        # Toolbar
        toolbar = self.addToolBar('Actions')

        toolbar.addAction(saveAction)
        toolbar.addAction(loadAction)
        toolbar.addAction(helpAction)
        toolbar.addAction(aboutAction)
        toolbar.addAction(exitAction)

        toolbar.setMovable(False)

    def closeEvent(self, event):
        """Custom close event"""
        reply = QtGui.QMessageBox.question(
            self, 'Message', 'Are you sure you want to quit!?',
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
            QtGui.QMessageBox.No
        )
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def center(self):
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def about(self):
        text = """Fatih Avcı
rinqtf@gmail.com
2019
Icon copyrights belong exclusively to Ray Cheung - WebAppers.com"""
        QtGui.QMessageBox.about(self, 'About', text)

    def saveme(self):
        text = pyperclip.paste()

        if text == '':
            QtGui.QMessageBox.information(
                self, 'Warning', 'Nothing to copy from!')
        else:
            labelNumber = 0
            while labelNumber < 1 or labelNumber > 8:
                labelNumber, isOk = QtGui.QInputDialog.getInt(
                    self, 'Select copy slot', 'Please enter a number between (1 - 8)')
                if not isOk:
                    break

            if isOk:
                numSet = figureNumber(labelNumber)
                if self.handleText.labels[numSet].toPlainText().strip() == 'Copy Text #{0}'.format(str(labelNumber)):
                    self.saveWrite(
                        self.handleText.labels[numSet], text, labelNumber)
                else:
                    reply = QtGui.QMessageBox.question(
                        self, 'Are you sure!?', 'Slot is not empty. Are you sure to overwrite!?',
                        QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                        QtGui.QMessageBox.No
                    )
                    if reply == QtGui.QMessageBox.Yes:
                        self.saveWrite(
                            self.handleText.labels[numSet], text, labelNumber)
                    else:
                        pass

    def loadme(self):
        labelNumber = 0
        while labelNumber < 1 or labelNumber > 8:
            labelNumber, isOk = QtGui.QInputDialog.getInt(
                self, 'Select load slot', 'Please enter a number between (1 - 8)')
            if not isOk:
                break

        if isOk:
            numSet = figureNumber(labelNumber)
            if self.handleText.labels[numSet].toPlainText().strip() != 'Copy Text #{0}'.format(str(labelNumber)):
                self.loadWrite(self.handleText.labels[numSet], labelNumber)
            else:
                QtGui.QMessageBox.information(self, 'Warning', 'Empty slot!')

    def saveWrite(self, item, text, num):
        item.setText('Copy Text #{0}\n{1}'.format(str(num), text))
        self.handleText.logBox.append(
            'Saved clipboard to #{0} - {1}'.format(
                str(num), time.asctime(time.localtime(time.time()))))

    def loadWrite(self, item, num):
        pyperclip.copy(item.toPlainText()[13:])
        self.handleText.descBox.setText(
            'Copied Text:\nFrom slot {0}\n'.format(num) + item.toPlainText()[13:])
        self.handleText.logBox.append(
            'Loaded text {0} to clipboard - {1}'.format(
                str(num), time.asctime(time.localtime(time.time()))))

    def helpme(self):
        text = f"""How to use this program:
* CTRL+C or copy text you want to use.
* To save text to program:
    * Switch to program and CTRL+S or File/Save or use save icon.
    * Enter slot number to save.
* To load text to clipboard:
    * CTRL+O or File/Load or use load icon.
"""
        QtGui.QMessageBox.information(self, 'Help', text)


class HandleText(QtGui.QWidget):
    def __init__(self):
        super(HandleText, self).__init__()
        self.controls()
        self.layout()
        self.actions()

    def controls(self):
        self.title = QtGui.QLabel('Choose your copy from: ')
        self.title.setFont(QtGui.QFont('comic sans', 14))

        self.labels = {}
        for i in range(4):
            for j in range(2):
                if j == 0:
                    self.labels[(i, j)] = QtGui.QTextEdit(
                        'Copy Text #' + str(i + j + 1) + '\n')
                else:
                    self.labels[(i, j)] = QtGui.QTextEdit(
                        'Copy Text #' + str(i + j + 4) + '\n')
                self.labels[(i, j)].setFixedSize(325, 150)
                self.labels[(i, j)].setFrameShape(QtGui.QFrame.Panel)
                self.labels[(i, j)].setLineWidth(3)
                self.labels[(i, j)].setAlignment(QtCore.Qt.AlignTop)
                self.labels[(i, j)].setReadOnly(True)

        self.descBox = QtGui.QTextEdit('Copied Text:\n')
        self.descBox.setFixedSize(300, 465)
        self.descBox.setFrameShape(QtGui.QFrame.Panel)
        self.descBox.setLineWidth(3)
        self.descBox.setAlignment(QtCore.Qt.AlignTop)
        self.descBox.setReadOnly(True)

        self.logBox = QtGui.QTextEdit('Log:\n')
        self.logBox.setFixedSize(300, 150)
        self.logBox.setFrameShape(QtGui.QFrame.Panel)
        self.logBox.setLineWidth(3)
        self.logBox.setAlignment(QtCore.Qt.AlignTop)
        self.logBox.setReadOnly(True)

    def layout(self):
        self.vbox = QtGui.QVBoxLayout()
        self.grid = QtGui.QGridLayout()
        self.hbox = QtGui.QHBoxLayout()
        self.vbox2 = QtGui.QVBoxLayout()

        self.vbox.addWidget(self.title)
        self.vbox.addLayout(self.hbox)
        self.hbox.addLayout(self.grid)
        self.hbox.addLayout(self.vbox2)
        self.vbox2.addWidget(self.descBox)
        self.vbox2.addWidget(self.logBox)

        for i in range(4):
            for j in range(2):
                self.grid.addWidget(self.labels[(i, j)], i, j)

        self.setLayout(self.vbox)

    def actions(self):
        # self.descBox
        pass

def main():
    app = QtGui.QApplication(sys.argv)
    GUI = Window()
    GUI.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
